# Stduino pcb Library
硬件资源免费开源给全球所有个人用户，个人使用者可在此基础上作任意修改及使用(包括商业使用)，但企业商业使用请联系service001@stduino.com

#### 介绍
Stduino系列芯片原理图及PCB板型开源库,欢迎大家积极反馈，让该项目持续为大家服务！

#### 硬件架构
 **Stduino Nano** 

   Stduino Nano 微控板使用了STM32F103C8T6芯片作为MCU，拥有32位高性能ARM核处理器，USB供电电压为5V，芯片的工作电压为2V-3.6V，工作温度为-40°C-85°C。引出数字引脚22个，模拟引脚8个，PWM引脚16个，UART串口3个，工作频率高达72MHz。实现了一键St-link\串口下载。
     

 **Stduino UNO** 

   Stduino Uno微控板使用了STM32F103C8T6芯片作为MCU,拥有32位高性能的ARM核处理器，USB供电电压为5V。芯片的工作电压为2V-3.6V，工作温度为-40°C-85°C，电源口输入电压6.2-18V。引出数字引脚18个，模拟引脚6个，PWM引脚14个，UART串口3个，工作频率高达72MHz。也实现了一键St-link\串口下载。




#### 使用说明

1. 有问题请直接官网反馈（官网：stduino.com）


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

